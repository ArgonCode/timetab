# Functionality

1. Timezone clocks
	* possibility to name them whatever
	* link to map with all cities
	* auto-order according to time
2. Time since
3. Time until
4. Calendar API for 2/3 ?
5. Star Wars countdown :-) , with death star image, get reverse version color

# Timeline / Task-line

* [x] Create function-less test chrome extension [https://developer.chrome.com/extensions/override](https://developer.chrome.com/extensions/override)
  * [x] can be installed
  * [x] links and loads all files and libraries
* [x] Test timezone libraries
* [x] Create tab page html/css/js with hard coded data
* [ ] Create settings page [https://developer.chrome.com/extensions/options](https://developer.chrome.com/extensions/options) and [https://github.com/frankkohlhepp/fancy-settings](https://github.com/frankkohlhepp/fancy-settings)


* [ ] Actual extension
  * [ ] get custom functionality bootstrap files to limit size
  * [ ] set how many since and how many until
  * [ ] how to read .cal data // or read it via google account ?
  * [ ] read data and timezone from the local machine
  * [ ] keep settings on the user's google account
  * [ ] maybe read location, if I find free weather API, worth the trouble
  * [ ] have an option for local time and set time. if you travel a lot

1. Get color library
2. Make reset button, in case user chooses something stupid or keep settings in one color ?
3. Add background photo link as settings possibility
