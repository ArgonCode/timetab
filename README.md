This is a repo of TimeTab. TimeTab is going to be a chrome new tab extension with a few functions that I like to have fast access to. It is a work in progress and the work is detailed in the [spec and journal file](https://github.com/ArgonCode/TimeTab/blob/master/specs_and_journal.md).


1. All timezones should be accounted for, but not all cities. Use moments.js map to see what is the location of your timezone. You can rename it as you wish. 
