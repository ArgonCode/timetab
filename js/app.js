var homeTime, timeDisplay, homeTimeLocalDisplay, timePassing;
var settings = {};
var getTimeZoneNames, addTimeZoneOptions, addTimeZonesEventListener, addTimeZonesNameEventListener;
var updateTimeZonesNames;
var updateFromSettings;
var getSettings, saveSettings, loadSavedSettings;

// Store basic settings
settings.timezones = new Array(9);
settings.timezonesNames = new Array(9);
settings.timeFormat = 'h:mm:ss a';
// TODO: can let user pick how many, in the future
// TODO: will have to populate html as well
// TODO: use new Array(number)


timePassing = function(timeType) {
  setInterval(function() {timeType();}, 1000);
};

// timeDisplay = function() {
//   homeTime = moment();
//   $('#home-time .hours').html(homeTime.format('HH') + " : ");
//   $('#home-time .minutes').html(homeTime.format('mm') + " : ");
//   $('#home-time .seconds').html(homeTime.format('ss'));
//   $('#home-date').html(homeTime.format('dddd, MMMM Do'));
// };


homeTimeLocalDisplay = function() {
  homeTime = moment();
  $('#home-time').html(homeTime.format('h:mm:ss a'));
  $('#home-date').html(homeTime.format('dddd, MMMM Do'));
};


// rewrite hometime display to all time display
// rewrite this into timeDisplay!
/*
TODO:
1. create time display of all time clocks
2. call it at the end of document ready
3. add it to save settings button
  * clear time interval, reset all data
  * set new time interval
  * use html ? not clear maybe ?
*/

timeDisplay = function() {
  var clocks = settings.timezones; // timezone clocks to display
  var names = settings.timezonesNames; // their desired names
  var timeFormat = settings.timeFormat; // time format display
  var clocksTimes = []; // array to sort timezones
  var clocksNames = {}; // timezones object => {"time": "name"}

  // populate clocksNames
  for(var r = 0; r < settings.timezones.length; r++){
    if(clocks[r]) {
      var key = moment().tz(clocks[r]).format(timeFormat); // the time is the key
      clocksTimes.push(key); // push the timeformats to an array
      var value = names[r];
      clocksNames[key] = value; // keep all in object
    }
  }

  // sort timeFormat keys
  clocksTimes.sort(function sortTime(a, b) {
    return a > b;
  });
  // TODO: move all of the above outside of ticking ?

  // attach time to DOM
  $(".additional-time").remove(); // clean old nodes

  for(j = 0; j < clocksTimes.length; j++) {
    var currentClockTime = clocksTimes[j]; // grab value from array, use as object key
    var currentClockName = clocksNames[currentClockTime]; // get name for clock to display
    $("#extra-times").append('<li class="flex-item additional-time">' + currentClockTime + "</br> " + currentClockName + '</li>');
  }

  // 1. get extra time from array
  // 2. push all of them to flex-items
  // 3. besically what i am doing now on button save
  // 4. call homeTime function for big clock inside interval
  // 5. they all have to tick at the same time
  // 6. Add day of the week for extra clocks
  // TODO: change color of the background if it is yesterday and tomorrow
  // TODO: sort object ?
  // DONE:  Add names or official names to timezonesNames array

  // all values are updated when chose, on button click they just have to be sorted
  // and displayed
  // call timer
  // hide settings
  // write a display time function outside of save settings function
  // settings functio only has to call display when changed, but otherwise display is called each time the tab opens up

  // updateFromSettings = function(){
  //   $("#update-time").click(function() {
  //     var timezones = settings.timezones;
  //     var timezonesNames = settings.timezonesNames;
  //     $(".additional-time").remove();
  //
  //     for(j = 0; j < timezones.length; j++) {
  //       if(timezones[j]) {
  //         var time = moment().tz(timezones[j]).format('h:mm:ss a');
  //         var name = timezonesNames[j];
  //         // TODO: make them tick!
  //         $("#extra-times").append('<li class="flex-item additional-time">' + time + "</br> " + name + '</li>');
  //       }
  //     }
  //   });
  // };
  //
  // homeTime = moment();
  // $('#home-time').html(homeTime.format('h : mm : ss a'));
  // $('#home-date').html(homeTime.format('dddd, MMMM Do'));

  // TODO: this one slows it down A LOT on every load. Get it out of her for first load
  // DONE: homeTimeLocalDisplay

  // $(".additional-time").remove();

  // var clocks =  storedData.settings.timezones;
  // for(var m = 0; m < clocks.length; m++) {
  //   if(clocks[m]) {
  //     var time = moment().tz(clocks[m]).format('h:mm:ss a');
  //     var name = timezonesNames[m];
  //     // TODO: make them tick!
  //     $("#extra-times").append('<li class="flex-item additional-time">' + time + "</br> " + name + '</li>');
  //   }
  // }
};


getSettings = function() {
  chrome.storage.sync.get(function(storedData) {
    // update global storage settings
    console.log("I am inside getSettings");
    settings = storedData.settings;
  });
};

// TODO THINK: javascript scope example, callback functions

saveSettings = function() {
  chrome.storage.sync.set({'settings': settings}, function(){
    // getSettings(); // TODO: do I need it here?
  });
};

/*##################################################
getSettings and saveSettings are ASYNC and the order
is not reliable
this function calls:
  getSettings();
  console.log("got settings", settings);
  saveSettings();
  getSettings();
  console.log("saved settings", settings);
produced this answer:
  got settings undefined    app.js:175
  saved settings undefined  app.js:179
  i am inside getSettings   app.js:61
  i am inside saveSettings  app.js:70
  i am inside getSettings   app.js:61
##################################################*/


loadSavedSettings = function() {
  chrome.storage.sync.get(function(storedData) {
    if(storedData.settings) {
      settings = storedData.settings; // global settings{} set to chrome stored
    } else {
      settings.backgroundColor = "#fff"; // TODO: implement color choice
      settings.clockFontColor = "#fff"; // TODO: implement color choice
      settings.clockBackgroundColor = "#FF6347"; // TODO: implement color choice
      settings.timeFormat = "HH : mm : ss"; // TODO: implement switching timezone am/pm: h : mm : ss a 24: HH : mm : ss
    }
  });
};






// ##################################################
// Settings page
// Populate TimeZones from TZ library to display in settings form
getTimeZoneNames = function (){
  timeZonesArray = moment.tz.names();
  timeZonesSelectString = '';
  for(var i = 0; i < timeZonesArray.length; i++) {
    timeZonesSelectString += '<option value="' + timeZonesArray[i] + '">' + timeZonesArray[i] + "</option>";
  }
  return timeZonesSelectString;
};

// add timezone options to drop-down selects
addTimeZoneOptions = function() {
  var timezonesString = getTimeZoneNames();
  for(var k = 0; k < settings.timezones.length; k++) {
    $('#timezones-options-' + k).append(timezonesString);
  }
  $('select').material_select();
};


// add listeners to clocks, to update placeholders when chosen
// TODO: redo without using .each() ?
addTimeZonesEventListener = function(){
  var timeZonesChoiceListener = function(num) {
    return $("#timezones-options-" + num).change(function() {
      $( "#timezones-options-" + num + " option:selected" ).each(function() {
        var selection = $(this).text();
        $("#timezones-name-" + num).attr("placeholder", selection); // display in placeholder
        settings.timezones[num] = selection; // keep in timezones array (for time change)
        settings.timezonesNames[num] = selection; // keep in the timezonesNames array (for name display)
      });
    });
  };
  for(var i = 0; i < settings.timezones.length; i++) {
    timeZonesChoiceListener(i);
  }
};

addTimeZonesNameEventListener = function(){
  $(".rename-time-zones").change(function() {
    var newName = $(this).val(); // value of the new name
    var location = parseInt($(this).attr("id").replace("timezones-name-", "")); // which timezone was changed
    settings.timezonesNames[location] = newName; // update name
    // TODO: change timezone name and changed name into an object ? or keep as aray with location?
  });
};

// Display timezones:
updateFromSettings = function(){
  $("#update-time").click(function() {
    var timezones = settings.timezones;
    var timezonesNames = settings.timezonesNames;
    $(".additional-time").remove();

    for(j = 0; j < timezones.length; j++) {
      if(timezones[j]) {
        var time = moment().tz(timezones[j]).format('h:mm:ss a');
        var name = timezonesNames[j];
        // TODO: make them tick!
        $("#extra-times").append('<li class="flex-item additional-time">' + time + "</br> " + name + '</li>');
      }
    }
  });
};
// ##################################################
var update = function(){
  $("#update-timeDisplay").click(timeDisplay);
};
// just attach it :)

// ##################################################




// ##################################################
$(document).ready(function() {
  homeTimeLocalDisplay(); // display local time on loaded
  // getSettings();
  saveSettings(); // push settings object to chrome storage

  addTimeZoneOptions(); // to drop-down selects
  addTimeZonesEventListener(); // to select to add on select
  addTimeZonesNameEventListener(); // event listeners for names to keep

  update();
  // updateFromSettings();



  setInterval(function() {timeDisplay();}, 1000);
  setInterval(function() {homeTimeLocalDisplay();}, 1000);

  // timePassing(timeDisplay); // it has to be the last thing, the way it is written now, it refreshes the time
});
