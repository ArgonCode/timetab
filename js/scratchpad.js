// define some test app settings
settings.backgroundColor = "#fff";
settings.fontColor = "#000";
settings.boxColor = "tomato";

// TODO: after choosing clock:
// change first option's value to empty, and activate it's selection

// save settings into chrome user storage
chrome.storage.sync.set({'settings': settings}, function(){
  console.log("Settings saved!");
});

// get settings from chrome user storage
chrome.storage.sync.get(function(storedData) {
  console.log(storedData.settings);
  console.log("background", storedData.settings.backgroundColor);
  console.log("box", storedData.settings.boxColor);
  console.log("font", storedData.settings.fontColor);
});
